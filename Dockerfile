FROM node:20-alpine AS builder

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install -g pnpm
COPY . .

RUN pnpm install --force
RUN pnpm build

# FROM nginx:alpine
# COPY --from=builder /usr/src/app/www /usr/share/
# COPY ./nginx.conf /etc/nginx/nginx.conf
# EXPOSE 80
# CMD ["nginx", "-g" "daemon off;"]
### STAGE 2:RUN ###

FROM nginx:latest AS ngx

COPY --from=builder /usr/src/app/www /usr/share/nginx/html
COPY /nginx.conf  /etc/nginx/nginx.conf

EXPOSE 80
