import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: 'home',
    loadComponent: () => import('./components/home/home.page'),
  },
  {
    path: 'note',
    loadComponent: () => import('./components/note/note.component'),
  },
  {
    path: 'message/:id',
    loadComponent: () => import('./components/view-message/view-message.page'),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];
