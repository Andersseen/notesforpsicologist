import { ApplicationConfig, isDevMode } from '@angular/core';
import { provideRouter } from '@angular/router';
import { routes } from './app.routes';
import { provideStoreDevtools } from '@ngrx/store-devtools';
import { provideStore } from '@ngrx/store';
import { appReducers } from './store/app.store';
import { provideIonicAngular } from '@ionic/angular/standalone';
import { DataService } from './services/data.service';

export const appConfig: ApplicationConfig = {
  providers: [
    DataService,
    provideIonicAngular(),
    provideRouter(routes),
    provideStore(appReducers),
    provideStoreDevtools({
      maxAge: 25,
      logOnly: !isDevMode(),
      connectInZone: true,
    }),
  ],
};
