import { Injectable, inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { Note, selectNotes } from '../store/note.store';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private store = inject(Store);

  public getNotes(): Note[] {
    const notes = this.store.selectSignal(selectNotes);
    const returnCopyOfNotes = [...notes()];
    return returnCopyOfNotes;
  }

  public getNoteById(id: string): Note | null {
    const notes = this.getNotes();
    const findNote = notes.find((item) => item.id === id);
    if (!findNote) return null;
    return findNote;
  }
}
