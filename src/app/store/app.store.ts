import { ActionReducerMap } from '@ngrx/store';
import { Note, notesReducer } from './note.store';

export interface AppState {
  notes: Note[];
}

export const appReducers: ActionReducerMap<AppState> = {
  notes: notesReducer,
};
