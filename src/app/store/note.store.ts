import {
  createAction,
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
  props,
} from '@ngrx/store';
import { v4 as uuidv4 } from 'uuid';
import { format } from 'date-fns';

export interface Note {
  id: string;
  title: string;
  subject: string;
  date: string;
  readed: boolean;
}
const dateFormate = 'yyyyMMddkkmm';

const initialState: Note[] = [
  {
    id: uuidv4(),
    title: 'First title',
    subject: '',
    date: format(new Date(), dateFormate),
    readed: false,
  },
  {
    id: uuidv4(),
    title: 'Second title',
    subject: '',
    date: format(new Date(), dateFormate),
    readed: false,
  },
];

export const addNote = createAction(
  '[Note] addNote',
  props<{ title: string; subject: string; date: string }>()
);
export const markNoteAsReaded = createAction(
  '[Note] markNoteAsCompleted',
  props<{ id: string }>()
);
export const removeNote = createAction(
  '[Note] removeNote',
  props<{ id: string }>()
);

export const notesReducer = createReducer(
  initialState,
  on(addNote, (state, { title, subject, date }) => [
    ...state,
    { title, subject, date, readed: false, id: uuidv4() },
  ]),
  on(markNoteAsReaded, (state, { id }) =>
    state.map((note) => {
      if (note.id === id) {
        return {
          ...note,
          readed: !note.readed,
        };
      }
      return note;
    })
  ),
  on(removeNote, (state, { id }) => state.filter((Note) => Note.id !== id))
);

const selectNotesFeature = createFeatureSelector<Note[]>('notes');

export const selectNotes = createSelector(
  selectNotesFeature,
  (state: Note[]) => state
);
