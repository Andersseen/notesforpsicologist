import {
  ChangeDetectionStrategy,
  Component,
  inject,
  Input,
} from '@angular/core';
import { RouterLink } from '@angular/router';
import {
  Platform,
  IonItem,
  IonLabel,
  IonNote,
  IonIcon,
} from '@ionic/angular/standalone';
import { format, parse } from 'date-fns';
import { addIcons } from 'ionicons';
import { chevronForward } from 'ionicons/icons';
import { Note } from 'src/app/store/note.store';

const showDateFormate = 'kk:mm | dd/MM/yy';
const dateFormate = 'yyyyMMddkkmm';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [RouterLink, IonItem, IonLabel, IonNote, IonIcon],
})
export default class MessageComponent {
  private platform = inject(Platform);
  @Input() note?: Note;
  isIos() {
    return this.platform.is('ios');
  }
  constructor() {
    addIcons({ chevronForward });
  }

  showDate(date) {
    return format(parse(date, dateFormate, new Date()), showDateFormate);
  }
}
