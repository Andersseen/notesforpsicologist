import { Component, inject } from '@angular/core';
import { FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  IonHeader,
  IonToolbar,
  IonButtons,
  IonBackButton,
  IonContent,
  IonInput,
  IonTextarea,
  IonList,
  IonItem,
  IonButton,
  IonRippleEffect,
} from '@ionic/angular/standalone';
import { Store } from '@ngrx/store';
import { format } from 'date-fns';
import { DataService } from 'src/app/services/data.service';
import { Note, addNote } from 'src/app/store/note.store';

@Component({
  selector: 'app-note',
  templateUrl: 'note.template.html',
  styleUrl: 'note.styles.scss',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    IonHeader,
    IonToolbar,
    IonButtons,
    IonBackButton,
    IonContent,
    IonList,
    IonItem,
    IonInput,
    IonTextarea,
    IonRippleEffect,
  ],
})
export default class NoteComponent {
  private store = inject(Store);
  private router = inject(Router);

  public noteForm = inject(FormBuilder).group({
    title: ['', Validators.required],
    subject: ['', Validators.required],
    date: [''],
  });

  saveNote() {
    this.noteForm.get('date').setValue(format(new Date(), 'yyyyMMddkkmm'));
    this.store.dispatch(addNote(this.noteForm.value as Note));
    this.noteForm.reset();
    this.router.navigate(['../']);
  }
}
