import { Component, inject } from '@angular/core';
import {
  RefresherCustomEvent,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonRefresher,
  IonRefresherContent,
  IonList,
  IonButton,
  IonIcon,
  IonButtons,
} from '@ionic/angular/standalone';
import { DataService } from '../../services/data.service';
import MessageComponent from '../message/message.component';
import { addIcons } from 'ionicons';
import { addCircleOutline } from 'ionicons/icons';
import { RouterLink } from '@angular/router';
import { Note } from 'src/app/store/note.store';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  standalone: true,
  imports: [
    RouterLink,
    IonHeader,
    IonIcon,
    IonButton,
    IonButtons,
    IonToolbar,
    IonTitle,
    IonContent,
    IonRefresher,
    IonRefresherContent,
    IonList,
    MessageComponent,
  ],
})
export default class HomePage {
  private data = inject(DataService);

  constructor() {
    addIcons({ addCircleOutline });
  }

  refresh(ev: any) {
    setTimeout(() => {
      (ev as RefresherCustomEvent).detail.complete();
    }, 3000);
  }

  get notes(): Note[] {
    return this.data.getNotes();
  }
}
